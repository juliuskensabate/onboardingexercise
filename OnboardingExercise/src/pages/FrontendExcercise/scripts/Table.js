import axios from "axios";
import { useRouter } from "vue-router";

export default {
  name: "MyTable",
  setup() {
    const router = useRouter();
    const navigateToForm = () => {
      router.push("/form");
    };

    const toggleActionsMenu = (row) => {
      row.showActions = !row.showActions;
    };

    const editEmployee = (row) => {
      router.push({ path: "/form", query: { id: row.id } });
    };

    const deleteEmployee = (row) => {
      console.log("Deleting employee:", row);

      axios
        .delete(`http://localhost:5000/users/${row.id}`)
        .then((response) => {
          const index = this.tableData.findIndex((item) => item.id === row.id);
          if (index !== -1) {
            this.tableData.splice(index, 1);
            console.log("Employee deleted. Refreshing route...");
            location.reload();
            console.log("Route refreshed.");
          }
        })
        .catch((error) => {
          console.error("Error deleting employee:", error);
        });
    };

    return {
      toggleActionsMenu,
      editEmployee,
      deleteEmployee,
      navigateToForm,
    };
  },
  data() {
    return {
      selectedDate: "",
      selectedOption: "",
      isDropdownOpen: false,
      isCalendarOpen: false,
      options: ["Page 1", "Page 2"],
      tableData: [],
      columns: [
        {
          name: "Employee ID",
          align: "left",
          label: "EmployeeID",
          field: "employee_id",
          sortable: true,
        },
        {
          name: "Name",
          align: "left",
          label: "Name",
          field: "name",
          sortable: true,
        },
        {
          name: "Email",
          align: "left",
          label: "Email",
          field: "email",
          sortable: true,
        },
        {
          name: "Task",
          align: "left",
          label: "Task",
          field: "currentTask",
          sortable: true,
        },
        {
          name: "Description",
          align: "left",
          label: "Description",
          field: "description",
          sortable: true,
        },
        {
          name: "Status",
          align: "left",
          label: "Status",
          field: "status",
          sortable: true,
        },
        {
          name: "Address",
          align: "left",
          label: "Address",
          field: "address",
          sortable: true,
        },
        {
          name: "ellipsis",
          align: "left",
          label: "",
          field: "ellipsis",
          sortable: false,
        },
      ],
    };
  },
  mounted() {
    axios
      .get("http://localhost:5000/users")
      .then((response) => {
        this.tableData = response.data;
      })
      .catch((error) => {
        console.error("Error fetching data:", error);
      });
  },
  methods: {
    toggleDropdown() {
      this.isDropdownOpen = !this.isDropdownOpen;
      this.isCalendarOpen = false;
    },
    selectOption(option) {
      this.selectedOption = option;
      this.isDropdownOpen = false;
    },
    toggleCalendar() {
      this.isCalendarOpen = !this.isCalendarOpen;
      this.isDropdownOpen = false;
    },
  },
};
