const routes = [
  {
    path: "/",
    redirect: {
      name: "dashboard",
    },
    component: () => import("layouts/MainLayout.vue"),
    children: [
      {
        path: "dashboard",
        name: "dashboard",
        component: () => import("pages/FrontendExcercise/DashboardPage.vue"),
      },
      {
        path: "table",
        name: "table",
        component: () => import("pages/FrontendExcercise/TablePage.vue"),
      },
      {
        path: "form",
        name: "form",
        component: () => import("pages/FrontendExcercise/FormPage.vue"),
      },
    ],
  },

  {
    path: "/:catchAll(.*)*",
    component: () => import("pages/ErrorNotFound.vue"),
  },
];

export default routes;
